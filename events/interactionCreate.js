const client = require("../index");
require("dotenv").config();
const { glob } = require("glob");
const { promisify } = require("util");
const globPromise = promisify(glob);

client.on("interactionCreate", async (interaction) => {

  // Slash Commands
  const slashCommands = await globPromise(`${process.cwd()}/commands/*/*.js`);
  const arrayOfSlashCommands = [];
  slashCommands.map((value) => {
    const file = require(value);
    const splitted = value.split("/");
    const directory = splitted[splitted.length - 2];

    if (!file?.name) return;

    const properties = { directory, ...file };
    client.slashCommands.set(file.name, properties);

    if (["MESSAGE", "USER"].includes(file.type)) delete file.description;
    arrayOfSlashCommands.push(file);
  });

  // Slash Command Handling
  if (interaction.isChatInputCommand()) {

    let commandData = []
    await arrayOfSlashCommands.forEach(command => {
      console.log(command.name)
      if (command.name == interaction.commandName) {
        commandData.push(command)
      }
    });

    let dataToProcess = JSON.stringify(commandData[0])
    let parsedData = JSON.parse(dataToProcess)

    if (parsedData.private == true) {
      await interaction.deferReply({ ephemeral: true }).catch(() => { });

    } else {
      await interaction.deferReply({ ephemeral: false }).catch(() => { });
    }

    const cmd = client.slashCommands.get(interaction.commandName);
    if (!cmd)
      return interaction.followUp({ content: "An error has occurred " });

    const args = [];

    for (let option of interaction.options.data) {
      if (option.type === "SUB_COMMAND") {
        if (option.name) args.push(option.name);
        option.options?.forEach((x) => {
          if (x.value) args.push(x.value);
        });
      } else if (option.value) args.push(option.value);
    }
    interaction.member = interaction.guild.members.cache.get(interaction.user.id);

    cmd.run(client, interaction, args);
  }

  // Context Menu Handling
  if (interaction.isContextMenuCommand()) {
    await interaction.deferReply({ ephemeral: false });
    const command = client.slashCommands.get(interaction.commandName);
    if (command) command.run(client, interaction);
  }

});
