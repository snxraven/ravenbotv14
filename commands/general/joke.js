const {
    EmbedBuilder
} = require('discord.js');
var giveMeAJoke = require('give-me-a-joke');

module.exports = {
    name: "joke",
    description: "Gets a funny joke",

    run: async (client, interaction) => {
        giveMeAJoke.getRandomDadJoke(function (joke) {

            if (joke.includes("?")){
                let jokeData = joke.split("?")
                const embed = new EmbedBuilder()
                .setColor("#FF0000")
                .setTitle("Here is your joke...")
                .setDescription(jokeData[0] + "?||" + jokeData[1] + "||")
                .setTimestamp()
                .setFooter({
                    text: `Requested by ${interaction.user.tag}`,
                    iconURL: `${interaction.user.displayAvatarURL()}`
                });
            interaction.editReply({
                embeds: [embed]
            });
            } else {
                const embed = new EmbedBuilder()
                .setColor("#FF0000")
                .setTitle("Here is your joke...")
                .setDescription(joke)
                .setTimestamp()
                .setFooter({
                    text: `Requested by ${interaction.user.tag}`,
                    iconURL: `${interaction.user.displayAvatarURL()}`
                });
            interaction.editReply({
                embeds: [embed]
            });
            }
        })
    },
};