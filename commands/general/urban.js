const {
  EmbedBuilder
} = require('discord.js');
var urban = require('urban')
const isNotDefined = require("is-not-defined");

module.exports = {
  name: "urban",
  description: "Searches Urban Dictionary",
  options: [{
      "name": "query",
      "description": "The thing you want to search for",
      "required": true,
      "type": 3 // 6 is type USER
  }],

  run: async (client, interaction) => {

      let searchWord = interaction.options._hoistedOptions[0].value
      search = urban(searchWord);

      search.first(function(data) {

          if (!isNotDefined(data)) {

              const embed = new EmbedBuilder()
                  .setColor("#FF0000")
                  .setTitle("Results for: " + searchWord)
                  .setDescription("Definition: " + data.definition)
                  .addFields({
                      name: 'Example',
                      value: data.example
                  })
                  .setTimestamp()
                  .setFooter({
                      text: `Requested by ${interaction.user.tag}`,
                      iconURL: `${interaction.user.displayAvatarURL()}`
                  });
              interaction.editReply({
                  embeds: [embed]
              });
          } else {
              return interaction.editReply("Sorry, no results were found!")
          }
      });
  },
};