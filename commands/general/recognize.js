const { EmbedBuilder } = require('discord.js');
const cmd = require('cmd-promise')
var fs = require('fs');

module.exports = {
  name: "recognize",
  description: "recognize an object within an image using TinyGrad",
  options: [{
    "name": "image",
    "description": "A 3 layer jpg",
    "required": true,
    "type": 11 // 6 is type USER
  }],
  run: async (client, interaction) => {
    let rand = Math.floor(Math.random() * 99999999999999);

    fileURL = interaction.options._hoistedOptions[0].attachment.url

    cmd(`wget -O images/${rand}.jpg ${fileURL} && recognize images/${rand}.jpg`).then(out => {
      (async () => {
        await interaction.editReply({
          files: [`images/${rand}.jpg`],
          content: out.stdout
        });
        fs.unlinkSync(`images/${rand}.jpg`);
      })();
    }).catch(err => {
      (async () => {
        await interaction.editReply({
          content: "There was an error, try another image."
        });
        fs.unlinkSync(`images/${rand}.jpg`);
      })();
    })

  },
};
