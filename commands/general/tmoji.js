const { EmbedBuilder } = require('discord.js');
const { getEmoji, getAllEmoji, getThemes } = require('random-text-meme');


module.exports = {
  name: "tmoji",
  description: "Returns a random Text Emoji",

  run: async (client, interaction) => {
      await interaction.editReply(getEmoji());
  },
};
