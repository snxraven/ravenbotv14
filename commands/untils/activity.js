const { EmbedBuilder } = require('discord.js');
var unirest = require('unirest');

module.exports = {
  name: "bored",
  description: "Find an activity to do when you are bored.",

  run: async (client, interaction) => {

    unirest
      .get('https://www.boredapi.com/api/activity')
      .headers({
        'Accept': 'application/json', 'Content-Type': 'application/json',
      })
      .then((response) => {  
        let data = response.body
        const embed = new EmbedBuilder()
          .setColor("#FF0000")
          .setTitle("Here is something to do!")
          .setDescription(data.activity)
          .setTimestamp()
          .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
        interaction.followUp({ embeds: [embed] });
      })
  },
};
