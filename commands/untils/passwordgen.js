const { EmbedBuilder } = require('discord.js');
const generator = require('generate-password');

module.exports = {
  name: "password-generator",
  description: "Generates a random secure password",
  private: true,
  options: [{
    name: "length",
    description: "Provide a number for how long to make the password.",
    required: true,
    type: 3 // 6 is type USER
  }],
  run: async (client, interaction) => {
    const { value: length } = interaction.options._hoistedOptions[0];
    const password = generator.generate({
      length,
      numbers: true,
      symbols: true,
      excludeSimilarCharacters: true
    });
    const embed = new EmbedBuilder()
      .setColor("#FF0000")
      .setTitle("Password Generated!")
      .setDescription(`Click to reveal: ||${password}||`)
      .setTimestamp()
      .setFooter({
        text: `Requested by ${interaction.user.tag}`,
        iconURL: `${interaction.user.displayAvatarURL()}`
      });
    interaction.editReply({ embeds: [embed] });
  }
};
