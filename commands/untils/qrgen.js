const { EmbedBuilder } = require('discord.js');
const { AttachmentBuilder } = require('discord.js');
const { AwesomeQR } = require("awesome-qr");
const fs = require("fs");

module.exports = {
  name: "qr",
  description: "Generates a QR image",
  options: [{
    "name": "text-data",
    "description": "The data you would like inside of the QR",
    "required": true,
    "type": 3 // 6 is type USER
  }],

  run: async (client, interaction) => {

    (async () => {
      let rand = Math.floor(Math.random() * 99999).toString();
      let text = interaction.options._hoistedOptions[0].value

      const background = fs.readFileSync("Terminal-icon.png");

      const buffer = await new AwesomeQR({
        text: text,
        size: 500,
        backgroundImage: background,

      }).draw();

      fs.writeFileSync("./images/" + rand + ".png", buffer);
      const file = new AttachmentBuilder('./images/' + rand + ".png");

      const embed = new EmbedBuilder()
      .setColor("#FF0000")
      .setTitle("Your Generated QR Code")
      .setTimestamp()
      .setImage('attachment://images/' + rand + ".png")

      .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
    interaction.editReply({ embeds: [embed], files: [file]  });
    })();

  },
};
