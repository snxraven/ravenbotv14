const { EmbedBuilder } = require('discord.js');

module.exports = {
  name: "8ball",
  description: "Ask a question, get a response.",
  options: [{
    "name": "question",
    "description": "The question you would like to ask.",
    "required": true,
    "type": 3 // 6 is type USER
}],
  run: async (client, interaction) => {
    let question = interaction.options._hoistedOptions[0].value

    function send(question, message){
      const embed = new EmbedBuilder()
      .setColor("#FF0000")
      .setTitle(question)
      .setDescription(message)
      .setTimestamp()
      .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
    interaction.editReply({ embeds: [embed] });
    }

    var answer = Math.floor(Math.random() * 8);

    if (answer === 0) {
      send(question, "it is certain")
    }
    else if (answer === 1) {
      send(question, "It is decidedly so");
    }
    else if (answer === 2) {
      send(question, "Reply hazy try again");
    }
    else if (answer === 3) {
      send(question, "Cannot predict now");
    }
    else if (answer === 4) {
      send(question, "Do not count on it");
    }
    else if (answer === 5) {
      send(question, "My sources say no");
    }
    else if (answer === 6) {
      send(question, "Outlook not so good");
    }
    else if (answer === 7) {
      send(question, "Signs point to yes");
    }
  },
};
